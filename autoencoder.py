# -*- coding: utf-8 -*-

""" Auto Encoder Example.
Using an auto encoder on MNIST handwritten digits.
References:
    Y. LeCun, L. Bottou, Y. Bengio, and P. Haffner. "Gradient-based
    learning applied to document recognition." Proceedings of the IEEE,
    86(11):2278-2324, November 1998.
Links:
    [MNIST Dataset] http://yann.lecun.com/exdb/mnist/
"""
from __future__ import division, print_function, absolute_import

import sys
import argparse
parser = argparse.ArgumentParser(description='Generate an autoencoder for given data')
parser.add_argument('-e', action="store", dest="training_epochs", type=int, default=1500, help="Number of training epochs (default 1500)")
parser.add_argument('-r', action="store", dest="alpha", type=float, default=0.0005, help="Regularisation coefficient (default 0.0005)")
parser.add_argument('-l', action="store", dest="learning_rate", type=float, default=0.01, help="Learning rate (default 0.01)")
parser.add_argument('-n', action="store",required=True, dest="res_n", help="label for output matrix (result_[n].mat)")

parser.add_argument('-d', action="store", dest="use_chkpt", type=int, default=1, help="Use stored checkpoint data. 0 for No, 1 for Yes. Default yes")
parser.add_argument('--layer1', action="store", dest="n_hidden_1", type=int, default=256, help="Number of nodes in hidden layer 1 (256)")
parser.add_argument('--layer2', action="store", dest="n_hidden_2", type=int, default=128, help="Number of nodes in hidden layer 2 (128)")
parser.add_argument('--layer3', action="store", dest="n_hidden_3", type=int, default=32, help="Number of nodes in hidden layer 3 (32)")
parser.add_argument('--layer4', action="store", dest="n_hidden_4", type=int, default=32, help="Number of nodes in hidden layer 4 (32)")
parser.parse_args()

import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import scipy.io as sio
import pdb
"""
data = sio.loadmat('taylorGreen4VesVC10.mat')
posx = data['posx']
posy = data['posy']

xn1 = posx.reshape(64,80004).T
yn1 = posy.reshape(64,80004).T

data = sio.loadmat('taylorGreen4VesVC1.mat')
posx = data['posx']
posy = data['posy']

xn = np.append(posx.reshape(64,80004).T, xn1, axis=0)
yn = np.append(posy.reshape(64,80004).T, yn1, axis=0)
for n in range(len(yn)):
    xn[n, :] = (xn[n, :] - np.mean(xn[n, :]))
    yn[n, :] = (yn[n, :] - np.mean(yn[n, :]))
    coords = np.vstack([xn[n, :], yn[n, :]])
    cov = np.cov(coords)
    evals, evecs = np.linalg.eig(cov)
    sort_indices = np.argsort(evals)[::-1]
    evec1, evec2 = evecs[:, sort_indices]
    x_v1, y_v1 = evec1  # Eigenvector with largest eigenvalue
    x_v2, y_v2 = evec2
    theta = np.tanh((x_v1)/(y_v1))  
    rotation_mat = np.matrix([[np.cos(theta), -np.sin(theta)],
                              [np.sin(theta), np.cos(theta)]])
    transformed_mat = rotation_mat * coords
    xn[n, :], yn[n, :] = transformed_mat.A
    ixmax = np.argmax(xn[n, :])
    xn[n, :] = np.append(xn[n, ixmax:], xn[n, :ixmax])   
    yn[n, :] = np.append(yn[n, ixmax:], yn[n, :ixmax])
    maxn     = np.max(( np.max(xn[n,:] - np.min(xn[n,:])),
                        np.max(yn[n,:] - np.min(yn[n,:])) ))
    yn[n, :] = (yn[n, :] - np.min(yn[n, :]))/maxn
    xn[n, :] = (xn[n, :] - np.min(xn[n, :]))/maxn

inp = np.append(xn,yn,axis=1)
data = {"inp":inp}
sio.savemat("modified_input.mat",data)
pdb.set_trace()
"""

data = sio.loadmat('flipped_modified_data.mat')
inp  = data['inp']
"""
for i,case in enumerate(inp):
    case = case/max(case)
    ixmax = np.argmax(case[:64])
    case[:64] = np.append(case[ixmax:64],case[:ixmax])
    case[64:] = np.append(case[ixmax+64:],case[64:ixmax+64])
    inp[i,:] = case

"""

for i,case in enumerate(inp):
    inp[i,:] = case - min(case)
    inp[i,:] = inp[i,:]/max(inp[i,:])

train = inp[10000:,:]
test  = inp[:10000,:]
# Parameters


#data = {"inp", inp}
#sio.savemat("data.mat", data)

alpha = parser.parse_args().alpha
use_chkpt = parser.parse_args().use_chkpt
learning_rate = parser.parse_args().learning_rate
training_epochs = parser.parse_args().training_epochs
save_to = "result" + parser.parse_args().res_n + ".mat"
# Network Parameters
n_hidden_1 = parser.parse_args().n_hidden_1 # 1st layer num features
n_hidden_2 = parser.parse_args().n_hidden_2 # 2nd layer num features
n_hidden_3 = parser.parse_args().n_hidden_3 # 3nd layer num features
n_hidden_4 = parser.parse_args().n_hidden_4 # 3nd layer num features
n_hidden_5 = 12
n_input = 128 # MNIST data input (img shape: 28*28)

# tf Graph input (only pictures)
X = tf.placeholder("float", [None, n_input])

weights = {
    'encoder_h1': tf.Variable(tf.random_normal([n_input, n_hidden_1], stddev=0.05), name="encoder_h1"),
    'encoder_h2': tf.Variable(tf.random_normal([n_hidden_1, n_hidden_2], stddev=0.05), name="encoder_h2"),
    'encoder_h3': tf.Variable(tf.random_normal([n_hidden_2, n_hidden_3], stddev=0.05), name="encoder_h3"),
    'encoder_h4': tf.Variable(tf.random_normal([n_hidden_3, n_hidden_4], stddev=0.05), name="encoder_h4"),
    'encoder_h5': tf.Variable(tf.random_normal([n_hidden_4, n_hidden_5], stddev=0.05), name="encoder_h5"),
    'decoder_h1': tf.Variable(tf.random_normal([n_hidden_5, n_hidden_4], stddev=0.05), name="decoder_h1"),
    'decoder_h2': tf.Variable(tf.random_normal([n_hidden_4, n_hidden_3], stddev=0.05), name="decoder_h2"),
    'decoder_h3': tf.Variable(tf.random_normal([n_hidden_3, n_hidden_2], stddev=0.05), name="decoder_h3"),
    'decoder_h4': tf.Variable(tf.random_normal([n_hidden_2, n_hidden_1], stddev=0.05), name="decoder_h4"),
    'decoder_h5': tf.Variable(tf.random_normal([n_hidden_1, n_input], stddev=0.05), name="decoder_h5"),
}
biases = {
    'encoder_b1': tf.Variable(tf.random_normal([n_hidden_1], stddev=0.05), name="encoder_b1"),
    'encoder_b2': tf.Variable(tf.random_normal([n_hidden_2], stddev=0.05), name="encoder_b2"),
    'encoder_b3': tf.Variable(tf.random_normal([n_hidden_3], stddev=0.05), name="encoder_b3"),
    'encoder_b4': tf.Variable(tf.random_normal([n_hidden_4], stddev=0.05), name="encoder_b4"),
    'encoder_b5': tf.Variable(tf.random_normal([n_hidden_5], stddev=0.05), name="encoder_b5"),
    'decoder_b1': tf.Variable(tf.random_normal([n_hidden_4], stddev=0.05), name="decoder_b1"),
    'decoder_b2': tf.Variable(tf.random_normal([n_hidden_3], stddev=0.05), name="decoder_b2"),
    'decoder_b3': tf.Variable(tf.random_normal([n_hidden_2], stddev=0.05), name="decoder_b3"),
    'decoder_b4': tf.Variable(tf.random_normal([n_hidden_1], stddev=0.05), name="decoder_b4"),
    'decoder_b5': tf.Variable(tf.random_normal([n_input], stddev=0.05), name="decoder_b5"),
}
saver = tf.train.Saver({
    'encoder_h1': weights['encoder_h1'],
    'encoder_h2': weights['encoder_h2'],
    'encoder_h3': weights['encoder_h3'],
    'encoder_h4': weights['encoder_h4'],
    'encoder_h5': weights['encoder_h5'],
    'decoder_h1': weights['decoder_h1'],
    'decoder_h2': weights['decoder_h2'],
    'decoder_h3': weights['decoder_h3'],
    'decoder_h4': weights['decoder_h4'],
    'decoder_h5': weights['decoder_h5'],
    'encoder_b1': biases['encoder_b1'],
    'encoder_b2': biases['encoder_b2'],
    'encoder_b3': biases['encoder_b3'],
    'encoder_b4': biases['encoder_b4'],
    'encoder_b5': biases['encoder_b5'],
    'decoder_b1': biases['decoder_b1'],
    'decoder_b2': biases['decoder_b2'],
    'decoder_b3': biases['decoder_b3'],
    'decoder_b4': biases['decoder_b4'],
    'decoder_b5': biases['decoder_b5'],
})

#pdb.set_trace()
# Building the encoder
def encoder(x):
    # Encoder Hidden layer with sigmoid activation #1
    layer_1 = (tf.add(tf.matmul(x, weights['encoder_h1']),
                                   biases['encoder_b1']))
    layer_1b = tf.maximum(-0.01*layer_1,layer_1)
    # Decoder Hidden layer with sigmoid activation #2
    layer_2 =  (tf.add(tf.matmul(layer_1b, weights['encoder_h2']),
                                   biases['encoder_b2']))
    layer_2b = tf.maximum(-0.01*layer_2,layer_2)

    layer_3 =  (tf.add(tf.matmul(layer_2b, weights['encoder_h3']),
                                   biases['encoder_b3']))
    layer_3b = tf.maximum(-0.01*layer_3,layer_3)

    layer_4 =  (tf.add(tf.matmul(layer_3b, weights['encoder_h4']),
                                   biases['encoder_b4']))
    layer_4b = tf.maximum(-0.01*layer_4,layer_4)
    layer_5 =  (tf.add(tf.matmul(layer_4b, weights['encoder_h5']),
                                   biases['encoder_b5']))
    layer_5b = tf.maximum(-0.01*layer_5,layer_5)
    return layer_5b


# Building the decoder
def decoder(x):
    # Encoder Hidden layer with sigmoid activation #1
    layer_1 =  (tf.add(tf.matmul(x, weights['decoder_h1']),
                                   biases['decoder_b1']))
    layer_1b = tf.maximum(-0.01*layer_1,layer_1)
    # Decoder Hidden layer with sigmoid activation #2
    layer_2 =  (tf.add(tf.matmul(layer_1b, weights['decoder_h2']),
                                   biases['decoder_b2']))
    layer_2b = tf.maximum(-0.01*layer_2,layer_2)
    
    layer_3 =  (tf.add(tf.matmul(layer_2b, weights['decoder_h3']),
                                   biases['decoder_b3']))
    layer_3b = tf.maximum(-0.01*layer_3,layer_3)

    layer_4 =  (tf.add(tf.matmul(layer_3b, weights['decoder_h4']),
                                   biases['decoder_b4']))
    layer_4b = tf.maximum(-0.01*layer_4,layer_4)

    layer_5 =  (tf.add(tf.matmul(layer_4b, weights['decoder_h5']),
                                   biases['decoder_b5']))
    layer_5b = tf.maximum(-0.01*layer_5,layer_5)
    return layer_5b

# Construct model
encoder_op = encoder(X)
decoder_op = decoder(encoder_op)

# Prediction
y_pred = decoder_op
# Targets (Labels) are the input data.
y_true = X

# Define loss and optimizer, minimize the squared error

regulariser = tf.add_n((tf.nn.l2_loss(weights['encoder_h1']), tf.nn.l2_loss(weights['encoder_h2']),
                        tf.nn.l2_loss(weights['encoder_h3']), tf.nn.l2_loss(weights['encoder_h4']),
                        tf.nn.l2_loss(weights['decoder_h1']), tf.nn.l2_loss(weights['decoder_h2']),
                        tf.nn.l2_loss(weights['decoder_h5']), tf.nn.l2_loss(weights['encoder_h5']),
                        tf.nn.l2_loss(weights['decoder_h3']), tf.nn.l2_loss(weights['decoder_h4'])))
#regulariser = 0

cost = tf.reduce_mean(tf.add(tf.pow(y_true - y_pred, 2), alpha*regulariser))

optimizer = tf.train.AdamOptimizer(learning_rate).minimize(cost)
#optimizer = tf.train.RMSPropOptimizer(learning_rate).minimize(cost)

# Initializing the variables
init = tf.global_variables_initializer()
costs = []
# Launch the graph
min_c = 1e-3
with tf.Session() as sess:
    sess.run(init)
    # Training cycle
    if use_chkpt == 1:
        saver.restore(sess, "./tmp_tiny/model.ckpt")
    for epoch in xrange(training_epochs):
        # Loop over all batches
        batch_xs = train
        # Run optimization op (backprop) and cost op (to get loss value)
        _, c = sess.run([optimizer, cost], feed_dict={X: batch_xs})
        # Display logs per epoch step
        costs.append(c)
        if epoch%100 == 0 and c <= min_c:
            min_c = c
            saver.save(sess, "./tmp_tiny/model.ckpt")
        if epoch % 5 == 0:
            print("Epoch:", '%04d' % (epoch+1),
                  "cost=", "{:.9f}".format(c))

    print("Optimization Finished!")
    saver.save(sess, "./tmp_tiny/model.ckpt")
    y_predict = sess.run(y_pred, feed_dict={X: train}) 
    
    y_predict_test = sess.run(y_pred, feed_dict={X: test}) 
    tosave = {  'training_y_act':train, 
                'training_y_pred':y_predict, 
                'type':"Leaky ReLu 128(input) -> 256 -> 256 -> 256 -> 256 -> 16 (-> 256 ... Decode)", 
                "learning_rate": learning_rate, 
                "training_epochs":training_epochs, 
                "alpha_reg": alpha, 
                'test_y_act':test, 
                'test_y_pred':y_predict_test, 
                'costs':costs}
    #save_path = saver.save(sess, "./tmp/model.ckpt")
    sio.savemat(save_to,tosave)
