import scipy.io as sio

import numpy as np

data = sio.loadmat('modified_data.mat')
inp  = data['inp'].T

for i,case in enumerate(inp):
    case = case/max(case)
    ixmax = np.argmax(case[:64])
    case[:64] = np.append(case[ixmax:64],case[:ixmax])
    case[64:] = np.append(case[ixmax+64:],case[64:ixmax+64])
    inp[i,:] = case
    if i%1000 is 0:
        print(i)

for i in range(160008):
    v0 = inp[:, 1] - inp[:, 0]
    v1 = inp[:, -1] - inp[:, 0]
    v1_u = v0 / np.linalg.norm(v0); v2_u = v1 / np.linalg.norm(v1)
    angle = np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))
    if angle < 0:
        inp = np.append(np.flipud(inp[:, 1:-1]), inp[:, 0])
    if i%1000 is 0:
        print(i)

sio.savemat('flipped_modified_data.mat', {'inp': inp})
