addpath('~/Ves2D/src')
path = '~/Documents/tayGreenVF20'; %%Path to input file(without .mat extension)
load(strcat(path, '.mat'))
OC = curve;
sz = size(xImp);
posx = reshape(xImp, sz(1), sz(2) * sz(3));
posy = reshape(yImp, sz(1), sz(2) * sz(3));

for i = 1:length(posx)
    %%% Centering the vesicle
    posx(:, i) = posx(:, i) - mean(posx(:, i));
    posy(:, i) = posy(:, i) - mean(posy(:, i));
    
    %%% Obtainig the angles of inclination and rotating vesicle
    theta1 = OC.getIncAngle([posx(:, i); posy(:, i)]);
    theta2 = OC.getIncAngle2([posx(:, i); posy(:, i)]);
    rotmat = [sin(theta2), -cos(theta2); -sin(theta1), cos(theta1)] / (cos(theta1)*sin(theta2) - cos(theta2)*sin(theta1));
    
    tmp = rotmat * [posx(:, i)'; posy(:, i)'];
    posx(:, i) = tmp(1, :);
    posy(:, i) = tmp(2, :);
    
    %%% Cycling the positions so that the first point is always at the
    %%% right extreme
    [~, ixmax] = max(posx(:, i));
    x = posx(:, i);
    y = posy(:, i);
    
    posx(:, i) = [x(ixmax:end);x(1:ixmax - 1)];
    posy(:, i) = [y(ixmax:end);y(1:ixmax - 1)];
    
    %%% Rearranging the data to store the points clockwise order
    v1 = [posx(end, i), posy(end, i)] - [posx(1, i), posy(1, i)];
    v2 = [posx(2, i), posy(2, i)] - [posx(1, i), posy(1, i)]; 
    v1_u = v1 / norm(v1);
    v2_u = v2 / norm(v2);
    angle = atan2(v1_u(2), v1_u(1)) - atan2(v2_u(2), v2_u(1));
    if angle < 0
        posx(:, i) = [posx(1, i); flipud(posx(2:end, i))];
        posy(:, i) = [posy(1, i); flipud(posy(2:end, i))];
    end

    %%% Normalising to make learning easier
    posx(:, i) = (posx(:, i) - min(posx(:, i))) / (max(posx(:, i) - min(posx(:, i))));
    posy(:, i) = (posy(:, i) - min(posy(:, i))) / (max(posy(:, i) - min(posy(:, i))));
end
inp = [posx; posy]';
save(strcat(path, '_mod.mat'), 'inp')