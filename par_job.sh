#!/bin/bash
#SBATCH -J job2           # job name
#SBATCH -o job2.o       # output and error file name (%j expands to jobID)
#SBATCH -N 3              # total number of mpi tasks requested
#SBATCH -n 3              # total number of mpi tasks requested
#SBATCH -p gpu     # queue (partition) -- normal, development, etc.
#SBATCH -t 00:30:00        # run time (hh:mm:ss) - 1.5 hours
#SBATCH --mail-user=harish02murali@gmail.com
#SBATCH --mail-type=begin  # email me when the job starts
#SBATCH --mail-type=end    # email me when the job finishes
rm hostlist.txt
ibrun -n 3 -o 0 bash script.sh
ibrun -n 3 -o 0 python /work/04900/mdoshi/maverick/auto_enc/results/par_main.py -r0 -n50 -l0.0001 -e2 -ps 1 -wk 2
wait

