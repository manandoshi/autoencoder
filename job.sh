#!/bin/bash
#SBATCH -J job1           # job name
#SBATCH -o job1.o       # output and error file name (%j expands to jobID)
#SBATCH -N 1              # total number of mpi tasks requested
#SBATCH -n 1              # total number of mpi tasks requested
#SBATCH -p gpu     # queue (partition) -- normal, development, etc.
#SBATCH -t 06:30:00        # run time (hh:mm:ss) - 1.5 hours
#SBATCH --mail-user=manan.doshi96@gmail.com
#SBATCH --mail-type=begin  # email me when the job starts
#SBATCH --mail-type=end    # email me when the job finishes

python /work/04901/harish96/maverick/autoencoder/autoencoder.py -n1 -l 0.000025 -r 1e-6 -d1 -e10000 --layer1 256 --layer2 256 --layer3 256 --layer4 256              # run the MPI executable named a.out
