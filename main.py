# -*- coding: utf-8 -*-

from __future__ import division, print_function, absolute_import

import sys
import argparse
parser = argparse.ArgumentParser(description='Generate an autoencoder for given data')
parser.add_argument('-e', action="store", dest="training_epochs", type=int, default=1500, help="Number of training epochs (default 1500)")
parser.add_argument('-r', action="store", dest="alpha", type=float, default=0.0005, help="Regularisation coefficient (default 0.0005)")
parser.add_argument('-l', action="store", dest="learning_rate", type=float, default=0.01, help="Learning rate (default 0.01)")
parser.add_argument('-n', action="store",required=True, dest="res_n", help="label for output matrix (result_[n].mat)")

parser.add_argument('-d', action="store", dest="use_chkpt", type=int, default=1, help="Use stored checkpoint data. 0 for No, 1 for Yes. Default yes")
parser.add_argument('--layer1', action="store", dest="n_hidden_1", type=int, default=256, help="Number of nodes in hidden layer 1 (256)")
parser.add_argument('--layer2', action="store", dest="n_hidden_2", type=int, default=256, help="Number of nodes in hidden layer 2 (128)")
parser.add_argument('--layer3', action="store", dest="n_hidden_3", type=int, default=256, help="Number of nodes in hidden layer 3 (32)")
parser.add_argument('--layer4', action="store", dest="n_hidden_4", type=int, default=256, help="Number of nodes in hidden layer 4 (32)")
parser.parse_args()

import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import scipy.io as sio
import pdb
'''
data = sio.loadmat('flipped_modified_data.mat')
inp  = data['inp']
for i,case in enumerate(inp):
#    print(min(case))
    inp[i,:] = case - min(case)
    inp[i,:] = inp[i,:]/max(inp[i,:])

v1 = inp[len(inp)/32:len(inp)/4,:]
v2 = inp[9*len(inp)/32:len(inp)/2,:]
v3 = inp[17*len(inp)/32:3*len(inp)/4,:]
v4 = inp[25*len(inp)/32:len(inp),:]

data = sio.loadmat('tayGreenVF20_mod.mat')
v5 = data['inp']
data = sio.loadmat('tayGreenVF25_mod.mat')
v6 = data['inp']

inp = np.concatenate((v1,v2,v3,v4,v5,v6),axis = 0)
np.random.shuffle(inp)
sio.savemat('inp.mat',{"inp":inp})
'''
data = sio.loadmat("inp.mat")
inp = data["inp"]
train = inp[len(inp)/10:,:]
test  = inp[:len(inp)/10,:]

# Parameters

alpha = parser.parse_args().alpha
use_chkpt = parser.parse_args().use_chkpt
learning_rate = parser.parse_args().learning_rate
training_epochs = parser.parse_args().training_epochs
save_to = "result" + parser.parse_args().res_n
global_step = tf.Variable(0, name='global_step', trainable=False)

# Network Parameters
n_hidden_1 = parser.parse_args().n_hidden_1 # 1st layer num features
n_hidden_2 = parser.parse_args().n_hidden_2 # 2nd layer num features
n_hidden_3 = parser.parse_args().n_hidden_3 # 3nd layer num features
n_hidden_4 = parser.parse_args().n_hidden_4 # 3nd layer num features
n_hidden_5 = 12
n_input = 128 # MNIST data input (img shape: 28*28)

# tf Graph input
X = tf.placeholder("float32", [None, n_input])

weights = {
    'encoder_h1': tf.Variable(tf.random_normal([n_input, n_hidden_1], stddev=0.05), name="encoder_h1"),
    'encoder_h2': tf.Variable(tf.random_normal([n_hidden_1, n_hidden_2], stddev=0.05), name="encoder_h2"),
    'encoder_h3': tf.Variable(tf.random_normal([n_hidden_2, n_hidden_3], stddev=0.05), name="encoder_h3"),
    'encoder_h4': tf.Variable(tf.random_normal([n_hidden_3, n_hidden_4], stddev=0.05), name="encoder_h4"),
    'encoder_h5': tf.Variable(tf.random_normal([n_hidden_4, n_hidden_5], stddev=0.05), name="encoder_h5"),
    'decoder_h1': tf.Variable(tf.random_normal([n_hidden_5, n_hidden_4], stddev=0.05), name="decoder_h1"),
    'decoder_h2': tf.Variable(tf.random_normal([n_hidden_4, n_hidden_3], stddev=0.05), name="decoder_h2"),
    'decoder_h3': tf.Variable(tf.random_normal([n_hidden_3, n_hidden_2], stddev=0.05), name="decoder_h3"),
    'decoder_h4': tf.Variable(tf.random_normal([n_hidden_2, n_hidden_1], stddev=0.05), name="decoder_h4"),
    'decoder_h5': tf.Variable(tf.random_normal([n_hidden_1, n_input], stddev=0.05), name="decoder_h5"),
}
biases = {
    'encoder_b1': tf.Variable(tf.random_normal([n_hidden_1], stddev=0.05), name="encoder_b1"),
    'encoder_b2': tf.Variable(tf.random_normal([n_hidden_2], stddev=0.05), name="encoder_b2"),
    'encoder_b3': tf.Variable(tf.random_normal([n_hidden_3], stddev=0.05), name="encoder_b3"),
    'encoder_b4': tf.Variable(tf.random_normal([n_hidden_4], stddev=0.05), name="encoder_b4"),
    'encoder_b5': tf.Variable(tf.random_normal([n_hidden_5], stddev=0.05), name="encoder_b5"),
    'decoder_b1': tf.Variable(tf.random_normal([n_hidden_4], stddev=0.05), name="decoder_b1"),
    'decoder_b2': tf.Variable(tf.random_normal([n_hidden_3], stddev=0.05), name="decoder_b2"),
    'decoder_b3': tf.Variable(tf.random_normal([n_hidden_2], stddev=0.05), name="decoder_b3"),
    'decoder_b4': tf.Variable(tf.random_normal([n_hidden_1], stddev=0.05), name="decoder_b4"),
    'decoder_b5': tf.Variable(tf.random_normal([n_input], stddev=0.05), name="decoder_b5"),
}
saver = tf.train.Saver({
    'encoder_h1': weights['encoder_h1'],
    'encoder_h2': weights['encoder_h2'],
    'encoder_h3': weights['encoder_h3'],
    'encoder_h4': weights['encoder_h4'],
    'encoder_h5': weights['encoder_h5'],
    'decoder_h1': weights['decoder_h1'],
    'decoder_h2': weights['decoder_h2'],
    'decoder_h3': weights['decoder_h3'],
    'decoder_h4': weights['decoder_h4'],
    'decoder_h5': weights['decoder_h5'],
    'encoder_b1': biases['encoder_b1'],
    'encoder_b2': biases['encoder_b2'],
    'encoder_b3': biases['encoder_b3'],
    'encoder_b4': biases['encoder_b4'],
    'encoder_b5': biases['encoder_b5'],
    'decoder_b1': biases['decoder_b1'],
    'decoder_b2': biases['decoder_b2'],
    'decoder_b3': biases['decoder_b3'],
    'decoder_b4': biases['decoder_b4'],
    'decoder_b5': biases['decoder_b5'],
})

# Building the encoder
def encoder(x):
    # Encoder Hidden layer
    layer_1 = (tf.add(tf.matmul(x, weights['encoder_h1']),
                                   biases['encoder_b1']))
    layer_1b = tf.maximum(-0.01*layer_1,layer_1)

    layer_2 =  (tf.add(tf.matmul(layer_1b, weights['encoder_h2']),
                                   biases['encoder_b2']))
    layer_2b = tf.maximum(-0.01*layer_2,layer_2)

    layer_3 =  (tf.add(tf.matmul(layer_2b, weights['encoder_h3']),
                                   biases['encoder_b3']))
    layer_3b = tf.maximum(-0.01*layer_3,layer_3)

    layer_4 =  (tf.add(tf.matmul(layer_3b, weights['encoder_h4']),
                                   biases['encoder_b4']))
    layer_4b = tf.maximum(-0.01*layer_4,layer_4)

    layer_5 =  (tf.add(tf.matmul(layer_4b, weights['encoder_h5']),
                                   biases['encoder_b5']))
    layer_5b = tf.maximum(-0.01*layer_5,layer_5)

    return layer_5b

# Building the decoder
def decoder(x):
    # Encoder Hidden layer
    layer_1 =  (tf.add(tf.matmul(x, weights['decoder_h1']),
                                   biases['decoder_b1']))
    layer_1b = tf.maximum(-0.01*layer_1,layer_1)

    layer_2 =  (tf.add(tf.matmul(layer_1b, weights['decoder_h2']),
                                   biases['decoder_b2']))
    layer_2b = tf.maximum(-0.01*layer_2,layer_2)
    
    layer_3 =  (tf.add(tf.matmul(layer_2b, weights['decoder_h3']),
                                   biases['decoder_b3']))
    layer_3b = tf.maximum(-0.01*layer_3,layer_3)

    layer_4 =  (tf.add(tf.matmul(layer_3b, weights['decoder_h4']),
                                   biases['decoder_b4']))
    layer_4b = tf.maximum(-0.01*layer_4,layer_4)

    layer_5 =  (tf.add(tf.matmul(layer_4b, weights['decoder_h5']),
                                   biases['decoder_b5']))
    layer_5b = tf.maximum(-0.01*layer_5,layer_5)

    return layer_5b

# Construct model
encoder_op = encoder(X)
decoder_op = decoder(encoder_op)

# Prediction
y_pred = decoder_op
# Targets (Labels) are the input data.
y_true = X

# Define loss and optimizer, minimize the squared error

regulariser = tf.add_n((tf.nn.l2_loss(weights['encoder_h1']), tf.nn.l2_loss(weights['encoder_h2']),
                        tf.nn.l2_loss(weights['encoder_h3']), tf.nn.l2_loss(weights['encoder_h4']),
                        tf.nn.l2_loss(weights['decoder_h1']), tf.nn.l2_loss(weights['decoder_h2']),
                        tf.nn.l2_loss(weights['decoder_h5']), tf.nn.l2_loss(weights['encoder_h5']),
                        tf.nn.l2_loss(weights['decoder_h3']), tf.nn.l2_loss(weights['decoder_h4'])))
#regulariser = 0
x        = tf.slice(y_pred,[0,0],[-1,64])
y        = tf.slice(y_pred,[0,64], [-1,64])
IK       = np.fft.fftfreq(64)*1j
IK       = IK.astype(np.dtype('complex64'))
temp     = tf.complex(y_pred,0.0)
temp2    = tf.multiply(IK,tf.fft(tf.slice(temp,[0,0],[-1,64])))
dbydx    = tf.real(tf.ifft(temp2))
dbydy    = tf.real(tf.ifft(tf.multiply(IK,tf.fft(tf.slice(tf.complex(y_pred,0.0),[0, 64],[-1,64])))))

length   = tf.reduce_sum(tf.sqrt(tf.add(tf.square(dbydx),tf.square(dbydy))))
area     = tf.reduce_sum(tf.add(tf.multiply(x,dbydy),-1*tf.multiply(y,dbydx)))

r_x      = tf.slice(y_true,[0,0],[-1,64])
r_y      = tf.slice(y_true,[0,64], [-1,64])
r_dbydx  = tf.real(tf.ifft(tf.multiply(IK,tf.fft(tf.slice(tf.complex(y_true,0.0),[0,0],[-1, 64])))))
r_dbydy  = tf.real(tf.ifft(tf.multiply(IK,tf.fft(tf.slice(tf.complex(y_true,0.0),[0, 64],[-1,64])))))
r_length = tf.reduce_sum(tf.sqrt(tf.add(tf.square(r_dbydx),tf.square(r_dbydy))))
r_area   = tf.reduce_sum(tf.add(tf.multiply(r_x,r_dbydy),-1*tf.multiply(r_y,r_dbydx)))

c1       = tf.add_n([tf.reduce_mean(tf.pow(y_true - y_pred, 2)), alpha*regulariser])
c3       = 0.0#*(tf.pow(length-r_length,2)) #1e-6
c2       = 5*tf.add_n([tf.reduce_mean(tf.square(dbydx)), tf.reduce_mean(tf.square(dbydy))])
c4       = 0.0#*(tf.pow(area-r_area,2)) #1e-2

cost     = tf.add_n([c1 , c2, c3, c4])

optimizer = tf.train.AdamOptimizer(learning_rate).minimize(cost, global_step=global_step)
#optimizer = tf.train.RMSPropOptimizer(learning_rate).minimize(cost)

# Initializing the variables
init = tf.global_variables_initializer()
costs = []

def load_pretrain(sess):
   saver.restore(sess,"./tmp_tiny/model.ckpt") 

sv = tf.train.Supervisor(logdir="./supervisor/", init_fn=load_pretrain)
# Launch the graph
res_sub = 0

with sv.managed_session() as sess:
    #sess.run(init)
    #saver.restore(sess,"./tmp_tiny/model.ckpt") 
    cst = sess.run([cost], feed_dict={X: train})
    #pdb.set_trace()
    print(cst)
    for epoch in xrange(training_epochs):
        if sv.should_stop():
            break

        batch_xs = train
        _, c = sess.run([optimizer, cost], feed_dict={X: batch_xs})
        costs.append(c)

        if epoch%100 == 0:
            min_c = c
            y_predict = sess.run(y_pred, feed_dict={X: train}) 
            y_predict_test = sess.run(y_pred, feed_dict={X: test}) 
            tosave = {  'training_y_act':train, 
                        'training_y_pred':y_predict, 
                        'type':"Leaky ReLu 128(input) -> 256 -> 256 -> 256 -> 256 -> 16 (-> 256 ... Decode)", 
                        "learning_rate": learning_rate, 
                        "training_epochs":training_epochs, 
                        "alpha_reg": alpha, 
                        'test_y_act':test, 
                        'test_y_pred':y_predict_test, 
                        'costs':costs}
            
            n = len(y_predict)
            rmse = np.linalg.norm(y_predict - train) / np.sqrt(n)
            print("RMS error in training =","{:.9f}".format(rmse))

            n = len(y_predict_test)
            rmse = np.linalg.norm(y_predict_test - test) / np.sqrt(n)
            print("RMS error on test data=","{:.9f}".format(rmse))
            sio.savemat(save_to+"_"+str(res_sub)+".mat",tosave)
            res_sub = res_sub + 1

        if epoch % 5 == 0:
            print("Epoch:", '%04d' % (epoch+1),
                  "cost=", "{:.9f}".format(c),)
                  #"reg_cost=", "{:.9f}".format(regularised_penalty),
                  #"roughness_cost=", "{:.9f}".format(roughness_penalty),)
    pdb.set_trace()
    print("Optimization Finished!")
    y_predict = sess.run(y_pred, feed_dict={X: train}) 
    
    y_predict_test = sess.run(y_pred, feed_dict={X: test}) 
    tosave = {  'training_y_act':train, 
                'training_y_pred':y_predict, 
                'type':"Leaky ReLu 128(input) -> 256 -> 256 -> 256 -> 256 -> 12 (-> 256 ... Decode)", 
                "learning_rate": learning_rate, 
                "training_epochs":training_epochs, 
                "alpha_reg": alpha, 
                'test_y_act':test, 
                'test_y_pred':y_predict_test, 
                'costs':costs}
    sio.savemat(save_to+".mat",tosave)
